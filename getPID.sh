#!/bin/bash

# create and populate array in 'PID_START TIME/DATE' format
# with results from grep search (search term passed on running script)
declare -a pids='('$(ps -ef  | grep -v 'grep' | grep -v $0 | grep $1 | awk '{print $2 "_" $5}')')'

# for length of array, append whether or not it was started today
for ((i=0; i<${#pids[@]}; i++)); do

 if [[ ${pids[i]: -5:1} == ":" ]] ; then
   pids[i]+="_Started Today" 
 else 
   pids[i]+="_Not Started Today"
 fi

# remove time/date from array element and print
echo ${pids[i]} | sed 's/_.*_/_/'
done

