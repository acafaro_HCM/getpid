# README #

This script takes an argument, grep's it, and returns only the PID's and whether or not it was started today.

It will not return PID's if they have 'grep' or 'bin' in their grep search results.

### How do I get set up? ###

Add this to the end of your '.bash_profile' file:
 
     alias pid='PATH_TO_SCRIPT'

great. now restart terminal or run:
         
     source PATH_TO_BASH_PROFILE 

excellent! now you can type 'pid SEARCH_TERM' and it will return only the PID's
and the process start times/dates in this format: 'PID_(Not )Started Today'.